<?php get_header(); ?>
<?php $template_directory_uri = get_template_directory_uri(); ?>
<style>

.container:before{
	right: 0;
	background: none !important;

}

.error404 h1 {
    font-size: 48px;
    margin: 0px;
    }

.error404 h1 span {
    font-size: 150px;
    clear: both;
    display: block;
    line-height: 150px;
    color: #272560;
}

.col_404 {
	max-width:1024px;
	margin: auto;
	display: flex;
	justify-content: space-around;
}

.col_404 div {
	/*display: flex;
	/justify-content: center;
	align-items: center;
	flex-direction: column;*/
}


.col_404 img {
	max-height: 400px;
}

  .col_404 p {
    font-size: 20px;
    font-weight: 600;
    margin: 0px;
}
.col_404 .search-submit {
	background: #272560;
	border: 1px solid #272560;
	padding: 10px 20px;
	color: #ffffff;
}

.col_404 .search-submit:hover {
    color: #272560;
    background-color: #FFF;
}

.col_404 input[type="search"] {

    color: #666;
    border: 1px solid #dbdbdb;
    padding: 10px 5px;
    width: 70%;
}

.whitespace {
	margin-top: 50px;
}
</style>

<div id="main-content" class="error404">
	<div class="container">
		<div id="content-area" class="clearfix">
			
				<article id="post-0" <?php post_class( 'et_pb_post not_found' ); ?>>
					<?php //get_template_part( 'includes/no-results', '404' ); ?>


				</article> <!-- .et_pb_post -->


				<div class="col_404">

						<div>
							<h1 class="page-title"> 404 <span>OOPS</span> something is not right!</h1>
							<br />
							<p>Please try our search below to find what you're looking for<p>
							<?php //echo get_search_form( $echo ); ?>
							<div>
								<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
								    <label>
								        
								        <input type="search" class="search-field"
								            placeholder="<?php echo esc_attr_x( 'Search …', 'placeholder' ) ?>"
								            value="<?php echo get_search_query() ?>" name="s"
								            title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" />
								    </label>
								    <input type="submit" class="search-submit"
								        value="<?php echo esc_attr_x( 'Search', 'submit button' ) ?>" />
								</form>
							</div>

						</div>
						<div>
							<img src="<?php echo $template_directory_uri . '/images/404.png'; ?>" />
						</div>

				</div>
				
				<div class="whitespace"></div>

			</div> <!-- #left-area -->
			<?php //get_sidebar(); ?>
		</div> <!-- #content-area -->
	</div> <!-- .container -->
</div> <!-- #main-content -->

<?php get_footer(); ?>