<?php if ( 'on' == et_get_option( 'divi_back_to_top', 'false' ) ) : ?>

	<span class="et_pb_scroll_top et-pb-icon"></span>

<?php endif;

if ( ! is_page_template( 'page-template-blank.php' ) ) : ?>

			<footer id="main-footer">


				<div class="container">
					<div class="footer_mail_block">
						<div class="footer_social_block">
							<h2>Connect with us</h2>
							<div class="social_blocks">

								<?php
								if ( false !== et_get_option( 'show_footer_social_icons', true ) ) {
									get_template_part( 'includes/social_icons', 'footer' );
								}


								?>

							</div>
						</div>
					</div>


					<div class="footer_newsletter_block">
						<h2>Sign up to find out more</h2>
						<div class="newsletter_block">
							<form enctype="multipart/form-data" method="post" class="frm-show-form " id="form_o72n" >
								<div>
										<input type="hidden" name="frm_action" value="create" />
										<input type="hidden" name="form_id" value="2" />
										<input type="hidden" name="frm_hide_fields_2" id="frm_hide_fields_2" value="" />
										<input type="hidden" name="form_key" value="o72n" />
										<input type="hidden" name="item_meta[0]" value="" />
										<input type="hidden" name="item_key" value="" />
										<input type="hidden" id="frm_submit_entry_2" name="frm_submit_entry_2" value="61b177fdac" /><input type="hidden" name="_wp_http_referer" value="/retail/" />
									<!--
									<input type="email" id="field_t405" name="item_meta[10]" value=""  data-reqmsg="This field cannot be blank." data-invmsg="Email Address is invalid"   /><input type="submit" name="btnsubmit" value="submit" />
									-->

									<div id="frm_field_10_container" class="frm_form_field form-field  frm_required_field frm_top_container">
										    <label for="field_t405" class="frm_primary_label">Email Address
										        <span class="frm_required">*</span>
										    </label>
										    <input type="email" id="field_t405" name="item_meta[10]" value=""  data-reqmsg="This field cannot be blank." data-invmsg="Email Address is invalid"  />

										    
										    
										</div>
										<input type="hidden" name="item_key" value="" />
										<div class="frm_submit">

										<input type="submit" value="Submit"  />
										<img class="frm_ajax_loading" src="http://fluoro.dev.taoscreative.com.au/wp-content/plugins/formidable/images/ajax_loader.gif" alt="Sending"/>

										</div>
								</div>


							</form>



					<!--			
					<div class="entry-content">
						<div class="newsletter">
								<div class="frm_forms " id="frm_form_2_container">
								<form enctype="multipart/form-data" method="post" class="frm-show-form " id="form_o72n" >
									<div class="frm_form_fields ">
									<fieldset>
										<legend class="frm_hidden">newsletter</legend>

										<input type="hidden" name="frm_action" value="create" />
										<input type="hidden" name="form_id" value="2" />
										<input type="hidden" name="frm_hide_fields_2" id="frm_hide_fields_2" value="" />
										<input type="hidden" name="form_key" value="o72n" />
										<input type="hidden" name="item_meta[0]" value="" />
										<input type="hidden" id="frm_submit_entry_2" name="frm_submit_entry_2" value="61b177fdac" /><input type="hidden" name="_wp_http_referer" value="/retail/" />
										<div id="frm_field_10_container" class="frm_form_field form-field  frm_required_field frm_top_container">
										    <label for="field_t405" class="frm_primary_label">Email Address
										        <span class="frm_required">*</span>
										    </label>
										    <input type="email" id="field_t405" name="item_meta[10]" value=""  data-reqmsg="This field cannot be blank." data-invmsg="Email Address is invalid"  />

										    
										    
										</div>
										<input type="hidden" name="item_key" value="" />
										<div class="frm_submit">

										<input type="submit" value="Submit"  />
										<img class="frm_ajax_loading" src="http://fluoro.dev.taoscreative.com.au/wp-content/plugins/formidable/images/ajax_loader.gif" alt="Sending"/>

										</div>
									</fieldset>
									</div>
								</form>
								</div>

						</div>
					</div>--> <!-- .entry-content -->




						</div>

					</div>
				</div>




				<div id="footer_disclaimer" style="display: none;">
					<div class="container clearfix">
						<p id="footer-info">Disclaimer: Information provided via this website is for educational and communication purposes only. The material presented is neither intended to convey the only, nor necessarily the best, method or procedure, but rather represents techniques and procedures used by Dr  G Cario.  Dr G Cario disclaims any and all liability for injury and/or other damages which result from an individual using techniques presented on this website. Dr G Cario, to the best of his knowledge, believes the information presented is current and applicable to work being done by Gynaecological surgeons worldwide.
					 </p>
					</div>
				</div>



				<?php //get_sidebar( 'footer' ); ?>

				<div id="footer-bottom" style="padding: 0 !important;">
					<div class="container clearfix">
				<?php
					if ( false !== et_get_option( 'show_footer_social_icons', true ) ) {
						//get_template_part( 'includes/social_icons', 'footer' );
					}

					//echo et_get_footer_credits();
				?>

					</div>	<!-- .container -->
				</div>

		<?php
			if ( has_nav_menu( 'footer-menu' ) ) : ?>

				<div id="et-footer-nav">
					<div class="container">
						<div class="foot-left">
						<?php
							wp_nav_menu( array(
								'theme_location' => 'footer-menu',
								'depth'          => '1',
								'menu_class'     => 'bottom-nav',
								'container'      => '',
								'fallback_cb'    => '',
							) );
						?>
						</div>

						<div class="foot-right">
							<ul class="bottom-nav">
								<li>
									<a href="#">
										<img src="/wp-content/uploads/2016/12/Fluoro_Medical_V5_adjusted_20.png" />
									</a>
								</li>
								<li>
									<a href="#">
										<img src="/wp-content/uploads/2016/12/Fluoro_Medical_V5_adjusted_17.png" />
									</a>
								</li>
								<li>
									Copyright Fluoro Medical. Powered by <a href="https://www.taoscreative.com.au/" target="_blank">TAOS</a> 
								</li>
							</ul>
						</div>
					</div>
				</div> <!-- #et-footer-nav -->






			<?php endif; ?>


			</footer> <!-- #main-footer -->
		</div> <!-- #et-main-area -->

<?php endif; // ! is_page_template( 'page-template-blank.php' ) ?>

	</div> <!-- #page-container -->

	<?php wp_footer(); ?>
</body>
</html>
