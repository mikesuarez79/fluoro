<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'fluoro16_fluoro1');


/** MySQL database username */
define('DB_USER', 'root');


/** MySQL database password */
define('DB_PASSWORD', '');


/** MySQL hostname */
define('DB_HOST', 'localhost');


/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');


/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ',|6Pe}},[XUJzJY2Bzn{{:QtY^~q_U$o^DZb&z:]K6:py3#EmK%q8HLH*In9T@8T');

define('SECURE_AUTH_KEY',  '!qtubD`-|9?Cch^w+{:}*$w_,US|*^fDr(bxk;+`tCm%(nq@lTikY]3g)s7M]T`o');

define('LOGGED_IN_KEY',    'TPr_bQXwF64b@L=J/7xrH;K/YFDJOt7:5%Z[2-i(kID~Q#%_b,RIe7IMv{qT{A>_');

define('NONCE_KEY',        '74]i[&gi7jJyM#8uatTdg^B;)z%|t;&ec!/:rl?HQhhjR,ga.4YGcOk5|P-vdn~D');

define('AUTH_SALT',        'Ogin~6|Yhh;gmJ-:D|t!(p8K6u@@Q%b}4|~*0?y@s2A:q;8e:$?/YX6?`%:++D91');

define('SECURE_AUTH_SALT', '_3n!zhLH$?n@O$vI>@P5ti5!m&+2.z$l*dRG<RWw?sN1206ub*2#emKtDe|h>G#&');

define('LOGGED_IN_SALT',   ';7*s;0^MZ1/:v(&?BZl#t%i0So OIRsD`$X{wx2Y}yWebiaoteh=zk9QV[7luLSm');

define('NONCE_SALT',       '4G.VIuPxjGxBwl-is&twXy<ErF-t>h&L0GVv8O>2z9HaB+& nQo~,=cUXMlCp%N]');


/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';


/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);
define('WP_MEMORY_LIMIT', '3000M');

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
